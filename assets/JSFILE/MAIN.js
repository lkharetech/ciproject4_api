$(document).ready(function () {
    // Fetch data for listing 
    function fetch_data() {
        $.ajax({
            url: Base_url + 'action',
            method: "POST",
            data: { data_action: 'fetch_all' },
            success: function (data) {
                $('tbody').html(data);
            }
        });
    }

    fetch_data();

    // View ADD DATA Modal
    $('#add_button').click(function () {
        $('#user_form')[0].reset();
        $('.modal-title').text("ADD USER");
        $('#action').val('ADD');
        $('#data_action').val("Insert");
        $('#userModal').modal('show');
    });
    // Validate Modal
    $("#user_form").validate({
        rules: {
            firstname: {
                required: true,
            },
            lastname: {
                required: true,
            }
        },
        messages: {
            firstname: {
                required: "First name can't be blank !!",
            },
            lastname: {
                required: "Last name can't be blank !!",
            }
        }
    });
    // Form submit action
    $(document).on('submit', '#user_form', function (event) {
        event.preventDefault();
        $.ajax({
            url: Base_url + 'action',
            method: "POST",
            data: $(this).serialize(),
            dataType: "json",
            success: function (data) {
                if (data.success) {
                    $('#user_form')[0].reset();
                    $('#userModal').modal('hide');
                    fetch_data();
                    if ($('#data_action').val() == "Insert") {
                        $('#success_message').html('<div class="alert alert-success">Data Inserted</div>');
                    }
                }
            }
        })
    });
    // Delete data
    $(document).on('click', '.delete', function () {
        var id = $(this).attr('id');
        if (confirm("Are you sure you want to delete this?")) {
            $.ajax({
                url: Base_url + 'action',
                method: "POST",
                data: { id: id, data_action: 'Delete' },
                dataType: "JSON",
                success: function (data) {
                    if (data.success) {
                        $('#success_message').html('<div class="alert alert-success">Data Deleted</div>');
                        fetch_data();
                    }
                }
            });
        }
    });
    // Edit data
    $(document).on('click', '.edit', function () {
        var id = $(this).attr('id');
        $.ajax({
            url: Base_url + 'action',
            method: "POST",
            data: { id: id, data_action: 'fetch_single' },
            dataType: "JSON",
            success: function (data) {
                $('#userModal').modal('show');
                $('#firstname').val(data.firstname);
                $('#lastname').val(data.lastname);
                $('.modal-title').text('EDIT USER');
                $('#id').val(data.id);
                $('#action').val('EDIT');
                $('#data_action').val('Edit');
            }
        });
    });
});
