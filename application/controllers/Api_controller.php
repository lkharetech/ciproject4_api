<?php
class Api_controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('main_model');
    }

    function index()
    {
        $data = $this->main_model->fetch_all();
        echo json_encode($data->result_array());
    }
    // Insert code
    function insert()
    {
        $array = array();

        $data = array(
            'firstname' => trim($this->input->post('firstname')),
            'lastname' => trim($this->input->post('lastname'))
        );

        $this->main_model->insert_api($data);
        $array = array(
            'success'  => true
        );
        echo json_encode($array, true);
    }
    // Delete Code
    function delete()
    {
        if ($this->input->post('id')) {
            if ($this->main_model->delete_single_user($this->input->post('id'))) {
                $array = array(
                    'success' => true
                );
            } else {
                $array = array(
                    'error' => true
                );
            }
            echo json_encode($array);
        }
    }
    // Fetch Single Code
    function fetch_single()
    {
        if ($this->input->post('id')) {
            $data = $this->main_model->fetch_single_user($this->input->post('id'));
            foreach ($data as $row) {
                $output['firstname'] = $row["firstname"];
                $output['lastname'] = $row["lastname"];
                $output['id'] = $row["id"];
            }
            echo json_encode($output);
        }
    }
    // Update Code
    function update()
    {
        $data = array(
            'firstname' => trim($this->input->post('firstname')),
            'lastname'  => trim($this->input->post('lastname'))
        );
        $this->main_model->update_api($this->input->post('id'), $data);
        $array = array(
            'success'  => true
        );
        echo json_encode($array, true);
    }
}
