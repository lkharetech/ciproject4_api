<?php
class Main_controller extends CI_Controller
{
    function index()
    {
        $this->load->view('main_page');
    }

    function action()
    {
        if ($this->input->post('data_action')) {
            $data_action = $this->input->post('data_action');

            // Insert code
            if ($data_action == "Insert") {
                $api_url = "http://localhost/Project4_CI_API/api_controller/insert";
                $form_data = array(
                    'firstname' => trim($this->input->post('firstname')),
                    'lastname' => trim($this->input->post('lastname'))
                );
                $client = curl_init($api_url);

                curl_setopt($client, CURLOPT_POST, true);
                curl_setopt($client, CURLOPT_POSTFIELDS, $form_data);
                curl_setopt($client, CURLOPT_RETURNTRANSFER, true);
                $response = curl_exec($client);

                curl_close($client);

                echo $response;
            }
            // Listing Code
            if ($data_action == "fetch_all") {
                $api_url = "http://localhost/Project4_CI_API/api_controller";

                $client = curl_init($api_url);

                curl_setopt($client, CURLOPT_RETURNTRANSFER, true);

                $response = curl_exec($client);

                curl_close($client);

                $result = json_decode($response);

                $output = '';

                if (count($result) > 0) {
                    foreach ($result as $key => $row) {
                        $output .= '
                        <tr>
                        <td>' . $row->firstname . '</td>
                        <td>' . $row->lastname . '</td>
                        <td>
                            <button type="button" class="edit" name="edit" id="' . $row->id . '">EDIT</button>
                            <button type="button" class="delete" name="delete" id="' . $row->id . '">DELETE</button>
                        </td>
                        </tr>';
                    }
                } else {
                    $output .= '
                     <tr>
                      <td colspan="4" align="center">No Data Found</td>
                     </tr>
                     ';
                }

                echo $output;
            }
            // Delete Code
            if ($data_action == "Delete") {
                $api_url = "http://localhost/Project4_CI_API/api_controller/delete";

                $form_data = array(
                    'id'  => $this->input->post('id')
                );

                $client = curl_init($api_url);

                curl_setopt($client, CURLOPT_POST, true);

                curl_setopt($client, CURLOPT_POSTFIELDS, $form_data);

                curl_setopt($client, CURLOPT_RETURNTRANSFER, true);

                $response = curl_exec($client);

                curl_close($client);

                echo $response;
            }
            // Fetch Single Data Code 
            if ($data_action == "fetch_single") {
                $api_url = "http://localhost/Project4_CI_API/api_controller/fetch_single";

                $form_data = array(
                    'id'  => $this->input->post('id')
                );

                $client = curl_init($api_url);

                curl_setopt($client, CURLOPT_POST, true);

                curl_setopt($client, CURLOPT_POSTFIELDS, $form_data);

                curl_setopt($client, CURLOPT_RETURNTRANSFER, true);

                $response = curl_exec($client);

                curl_close($client);

                echo $response;
            }
            // Update & edit data Code 
            if ($data_action == "Edit") {
                $api_url = "http://localhost/Project4_CI_API/api_controller/update";

                $form_data = array(
                    'firstname'  => $this->input->post('firstname'),
                    'lastname'   => $this->input->post('lastname'),
                    'id'    => $this->input->post('id')
                );

                $client = curl_init($api_url);

                curl_setopt($client, CURLOPT_POST, true);

                curl_setopt($client, CURLOPT_POSTFIELDS, $form_data);

                curl_setopt($client, CURLOPT_RETURNTRANSFER, true);

                $response = curl_exec($client);

                curl_close($client);

                echo $response;
            }
        }
    }
}
