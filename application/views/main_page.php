<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <title>REST API CRUD</title>
    <style>
        table,
        th,
        td {
            border: 1px solid black;
            border-collapse: collapse;
        }

    </style>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
</head>

<body>
    <div class="container">
        <h2 style="text-align: center;">REST API CI CRUD</h2>
        <hr>
        <div>
            <button type="button" id="add_button">ADD DATA</button>
        </div>
        <br>
        <div>
            <span id="success_message"></span>
        </div>
        <hr>
        <div>
            <h4 style="text-align: center;">LISTING OF DATA</h4>
        </div>
        <div>
            <table>
                <thead>
                    <tr>
                        <th width="40%">FIRST NAME</th>
                        <th width="40%">LAST NAME</th>
                        <th width="20%">ACTION</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>

</body>

</html>

<div id="userModal" class="modal fade">
    <div class="modal-dialog">
        <form action="post" id="user_form">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add User</h4>
                </div>
                <div class="modal-body">
                    <label>Enter First Name :</label>
                    <input type="text" name="firstname" id="firstname" placeholder="Enter your first name" class="form-control">
                    <br><br>
                    <label>Enter Last Name :</label>
                    <input type="text" name="lastname" id="lastname" placeholder="Enter your last name" class="form-control">
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="id" id="id">
                    <input type="hidden" name="data_action" id="data_action" value="Insert">
                    <input type="submit" name="action" id="action" value="ADD">
                    <button type="button" data-dismiss="modal">CLOSE</button>
                </div>
            </div>
        </form>
    </div>
</div>

<base href="http://localhost/Project4_CI_API/">
<script>
    var Base_url = "<?php echo base_url(); ?>";
</script>
<script src="assets\JSFILE\MAIN.js"></script>