<?php
class Main_model extends CI_Model
{
    // Query for insert
    function insert_api($data)
    {
        $this->db->insert('users', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    // Query for listing
    function fetch_all()
    {
        $this->db->order_by('id', 'DESC');
        return $this->db->get('users');
    }
    // Query for delete
    function delete_single_user($id)
    {
        $this->db->where("id", $id);
        $this->db->delete("users");
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    // Query for single case 
    function fetch_single_user($id)
    {
        $this->db->where("id", $id);
        $query = $this->db->get('users');
        return $query->result_array();
    }
    //  Query for update details
    function update_api($id, $data)
    {
        $this->db->where("id", $id);
        $this->db->update("users", $data);
    }
}
